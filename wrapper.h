/*
 * wrapper.h
 *
 *  Created on: 19 ���� 2014
 *      Author: Inbal
 */

#ifndef WRAPPER_H_
#define WRAPPER_H_

#include <errno.h>

static inline int __generic_count_sons(unsigned int option, int pid) {
	unsigned long res;
	__asm__
	(
			"int $0x80;"
			: "=a" (res)
			: "0" (option ? 244 : 243), "b" (pid)
			: "memory"
	);
	if (res >= (unsigned long) (-125)) {
		errno = -res;
		res = -1;
	}
	return (int) res;
}

static inline int lshort_query_remaining_time(int pid) {
	return __generic_count_sons(0, pid);
}

static inline int lshort_query_overdue_time(int pid) {
	return __generic_count_sons(1, pid);
}

#endif /* WRAPPER_H_ */
